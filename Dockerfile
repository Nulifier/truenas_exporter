FROM golang:1.21 as base

WORKDIR /app/

COPY . .

RUN go mod download
RUN go mod verify

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /truenas_exporter .

FROM gcr.io/distroless/static-debian12:nonroot

COPY --from=base /truenas_exporter /truenas_exporter

EXPOSE 9876/tcp

ENTRYPOINT ["/truenas_exporter"]
CMD []
