# truenas_exporter
Prometheus exporter for TrueNAS Scale metrics to convert from NetData metrics.

It exposes a `/metrics` endpoint to scrape the metrics from. When scraped, it
queries the API and reports on the latest metrics. *Note:* Since the metrics are
not reported as counters by TrueNAS and only averages per second, almost all the
metrics from this exporter are reported as gauges with the `_per_second` suffix.

This has been tested on `TrueNAS-Scale-23.10.0.1`.

## Installation and Usage
`truenas_exporter` is distributed as either a single Go executable or a Docker
image.

You will need to know the URL of your TrueNAS installation. It should be of the
form `https://truenas.example.com`. Set this as the value of the `TRUENAS_URL`
variable.

You will also need an API key. To obtain an API key, go to the UI of your
TrueNAS install and click on the user profile button at the top right. Click on
"API Keys" and create a new API key. Copy the key and set it as the value of the
`TRUENAS_API_KEY` variable.

### Executable
Download the latest
[release](https://gitlab.com/Nulifier/truenas_exporter/-/releases). Configure
it using the comon flags and environmental variables [below](#configuration).

### Docker
```bash
docker run -d \
  --env TRUENAS_URL https://truenas.example.com \
  --env TRUENAS_API_KEY abc123 \
  -p 9876:9876/tcp \
  registry.gitlab.com/nulifier/truenas_exporter:latest
```

## Configuration
The configuration of `truenas_exporter` is fairly simple and more options may be
added in the future. In order to support Docker secrets, any of the
environmental variables can have `_FILE` appened to them and the value will be
read from the file instead of the variable. If both are specified, then the
`_FILE` version takes precedence.

### Flags
| Flag             | Default | Description                              |
| ---------------- | ------- | ---------------------------------------- |
| -listen-addresss | :9876   | Address the server listens on            |
| -verbose         | false   | Provides extra information while running |

### Environmental Variables
| Variable                   | Default | Description                                  |
| -------------------------- | ------- | -------------------------------------------- | 
| TRUENAS_URL                |         | URL of the TrueNAS server.                   |
| TRUENAS_API_KEY            |         | API key for the TrueNAS server.              |
| TRUENAS_DISK_TEMP_INTERVAL | 60      | How often the disk temperatures are updated. |

Due to a quirk of the TrueNAS Scale API, the disk temperature is only reported
around every 60 seconds. As this exporter only pulls the latest metrics when
scraped, it can miss reporting these disk temperatures. In order to always have
a disk temperature, it requests two times the last `TRUENAS_DISK_TEMP_INTERVAL`
and picks the last value.

## Metrics
Here is a list of the metrics exported:
```prometheus
# HELP go_gc_duration_seconds A summary of the pause duration of garbage collection cycles.
# TYPE go_gc_duration_seconds summary
go_gc_duration_seconds{quantile="0"} 0
go_gc_duration_seconds{quantile="0.25"} 0
go_gc_duration_seconds{quantile="0.5"} 0
go_gc_duration_seconds{quantile="0.75"} 0
go_gc_duration_seconds{quantile="1"} 0
go_gc_duration_seconds_sum 0
go_gc_duration_seconds_count 0
# HELP go_goroutines Number of goroutines that currently exist.
# TYPE go_goroutines gauge
go_goroutines 6
# HELP go_info Information about the Go environment.
# TYPE go_info gauge
go_info{version="go1.21.5"} 1
# HELP go_memstats_alloc_bytes Number of bytes allocated and still in use.
# TYPE go_memstats_alloc_bytes gauge
go_memstats_alloc_bytes 308000
# HELP go_memstats_alloc_bytes_total Total number of bytes allocated, even if freed.
# TYPE go_memstats_alloc_bytes_total counter
go_memstats_alloc_bytes_total 308000
# HELP go_memstats_buck_hash_sys_bytes Number of bytes used by the profiling bucket hash table.
# TYPE go_memstats_buck_hash_sys_bytes gauge
go_memstats_buck_hash_sys_bytes 4273
# HELP go_memstats_frees_total Total number of frees.
# TYPE go_memstats_frees_total counter
go_memstats_frees_total 0
# HELP go_memstats_gc_sys_bytes Number of bytes used for garbage collection system metadata.
# TYPE go_memstats_gc_sys_bytes gauge
go_memstats_gc_sys_bytes 2.483784e+06
# HELP go_memstats_heap_alloc_bytes Number of heap bytes allocated and still in use.
# TYPE go_memstats_heap_alloc_bytes gauge
go_memstats_heap_alloc_bytes 308000
# HELP go_memstats_heap_idle_bytes Number of heap bytes waiting to be used.
# TYPE go_memstats_heap_idle_bytes gauge
go_memstats_heap_idle_bytes 2.490368e+06
# HELP go_memstats_heap_inuse_bytes Number of heap bytes that are in use.
# TYPE go_memstats_heap_inuse_bytes gauge
go_memstats_heap_inuse_bytes 1.376256e+06
# HELP go_memstats_heap_objects Number of allocated objects.
# TYPE go_memstats_heap_objects gauge
go_memstats_heap_objects 610
# HELP go_memstats_heap_released_bytes Number of heap bytes released to OS.
# TYPE go_memstats_heap_released_bytes gauge
go_memstats_heap_released_bytes 2.490368e+06
# HELP go_memstats_heap_sys_bytes Number of heap bytes obtained from system.
# TYPE go_memstats_heap_sys_bytes gauge
go_memstats_heap_sys_bytes 3.866624e+06
# HELP go_memstats_last_gc_time_seconds Number of seconds since 1970 of last garbage collection.
# TYPE go_memstats_last_gc_time_seconds gauge
go_memstats_last_gc_time_seconds 0
# HELP go_memstats_lookups_total Total number of pointer lookups.
# TYPE go_memstats_lookups_total counter
go_memstats_lookups_total 0
# HELP go_memstats_mallocs_total Total number of mallocs.
# TYPE go_memstats_mallocs_total counter
go_memstats_mallocs_total 610
# HELP go_memstats_mcache_inuse_bytes Number of bytes in use by mcache structures.
# TYPE go_memstats_mcache_inuse_bytes gauge
go_memstats_mcache_inuse_bytes 19200
# HELP go_memstats_mcache_sys_bytes Number of bytes used for mcache structures obtained from system.
# TYPE go_memstats_mcache_sys_bytes gauge
go_memstats_mcache_sys_bytes 31200
# HELP go_memstats_mspan_inuse_bytes Number of bytes in use by mspan structures.
# TYPE go_memstats_mspan_inuse_bytes gauge
go_memstats_mspan_inuse_bytes 47376
# HELP go_memstats_mspan_sys_bytes Number of bytes used for mspan structures obtained from system.
# TYPE go_memstats_mspan_sys_bytes gauge
go_memstats_mspan_sys_bytes 48888
# HELP go_memstats_next_gc_bytes Number of heap bytes when next garbage collection will take place.
# TYPE go_memstats_next_gc_bytes gauge
go_memstats_next_gc_bytes 4.194304e+06
# HELP go_memstats_other_sys_bytes Number of bytes used for other system allocations.
# TYPE go_memstats_other_sys_bytes gauge
go_memstats_other_sys_bytes 945215
# HELP go_memstats_stack_inuse_bytes Number of bytes in use by the stack allocator.
# TYPE go_memstats_stack_inuse_bytes gauge
go_memstats_stack_inuse_bytes 327680
# HELP go_memstats_stack_sys_bytes Number of bytes obtained from system for stack allocator.
# TYPE go_memstats_stack_sys_bytes gauge
go_memstats_stack_sys_bytes 327680
# HELP go_memstats_sys_bytes Number of bytes obtained from system.
# TYPE go_memstats_sys_bytes gauge
go_memstats_sys_bytes 7.707664e+06
# HELP go_threads Number of OS threads created.
# TYPE go_threads gauge
go_threads 7
# HELP process_cpu_seconds_total Total user and system CPU time spent in seconds.
# TYPE process_cpu_seconds_total counter
process_cpu_seconds_total 0
# HELP process_max_fds Maximum number of open file descriptors.
# TYPE process_max_fds gauge
process_max_fds 1.048576e+06
# HELP process_open_fds Number of open file descriptors.
# TYPE process_open_fds gauge
process_open_fds 12
# HELP process_resident_memory_bytes Resident memory size in bytes.
# TYPE process_resident_memory_bytes gauge
process_resident_memory_bytes 1.2673024e+07
# HELP process_start_time_seconds Start time of the process since unix epoch in seconds.
# TYPE process_start_time_seconds gauge
process_start_time_seconds 1.70455846148e+09
# HELP process_virtual_memory_bytes Virtual memory size in bytes.
# TYPE process_virtual_memory_bytes gauge
process_virtual_memory_bytes 1.652047872e+09
# HELP process_virtual_memory_max_bytes Maximum amount of virtual memory available in bytes.
# TYPE process_virtual_memory_max_bytes gauge
process_virtual_memory_max_bytes 1.8446744073709552e+19
# HELP promhttp_metric_handler_errors_total Total number of internal errors encountered by the promhttp metric handler.
# TYPE promhttp_metric_handler_errors_total counter
promhttp_metric_handler_errors_total{cause="encoding"} 0
promhttp_metric_handler_errors_total{cause="gathering"} 0
# HELP truenas_arc_actual_events_per_second ZFS actual cache hit/miss rate
# TYPE truenas_arc_actual_events_per_second gauge
truenas_arc_actual_events_per_second{result="hits"} 227.2702
truenas_arc_actual_events_per_second{result="misses"} 0
# HELP truenas_arc_events_per_second ZFS cache hit/miss rate
# TYPE truenas_arc_events_per_second gauge
truenas_arc_events_per_second{result="hits"} 227.2638
truenas_arc_events_per_second{result="misses"} 0
# HELP truenas_arc_result_percentage ZFS ARC result percentage by category
# TYPE truenas_arc_result_percentage gauge
truenas_arc_result_percentage{category="demand_data",result="hits"} 1
truenas_arc_result_percentage{category="demand_data",result="misses"} 0
truenas_arc_result_percentage{category="prefetch_data",result="hits"} 0
truenas_arc_result_percentage{category="prefetch_data",result="misses"} 0
# HELP truenas_arc_size_bytes ZFS ARC size in bytes
# TYPE truenas_arc_size_bytes counter
truenas_arc_size_bytes 10400.525
# HELP truenas_cpu_temperature_celsius Temperature of the CPU core
# TYPE truenas_cpu_temperature_celsius gauge
truenas_cpu_temperature_celsius{cpu="0"} 38
truenas_cpu_temperature_celsius{cpu="1"} 38
truenas_cpu_temperature_celsius{cpu="2"} 38
truenas_cpu_temperature_celsius{cpu="3"} 38
# HELP truenas_cpu_usage_ratio Average time the CPU spend in a mode
# TYPE truenas_cpu_usage_ratio gauge
truenas_cpu_usage_ratio{mode="idle"} 0.9662078
truenas_cpu_usage_ratio{mode="iowait"} 0
truenas_cpu_usage_ratio{mode="nice"} 0.001251564
truenas_cpu_usage_ratio{mode="softirq"} 0.001251564
truenas_cpu_usage_ratio{mode="system"} 0.011264080000000001
truenas_cpu_usage_ratio{mode="user"} 0.02002503
# HELP truenas_disk_read_bytes_per_second Bytes read from disk per second by device
# TYPE truenas_disk_read_bytes_per_second gauge
truenas_disk_read_bytes_per_second{device="nvme0n1"} 0
truenas_disk_read_bytes_per_second{device="sda"} 0
truenas_disk_read_bytes_per_second{device="sdb"} 0
truenas_disk_read_bytes_per_second{device="sdc"} 0
truenas_disk_read_bytes_per_second{device="sdd"} 0
truenas_disk_read_bytes_per_second{device="sde"} 0
truenas_disk_read_bytes_per_second{device="sdf"} 0
truenas_disk_read_bytes_per_second{device="sdg"} 0
truenas_disk_read_bytes_per_second{device="sdh"} 0
# HELP truenas_disk_temperature_celsius Disk temperature by device in celsius
# TYPE truenas_disk_temperature_celsius gauge
truenas_disk_temperature_celsius{device="nvme0n1"} 38
truenas_disk_temperature_celsius{device="sda"} 26
truenas_disk_temperature_celsius{device="sdb"} 25
truenas_disk_temperature_celsius{device="sdc"} 26
truenas_disk_temperature_celsius{device="sdd"} 24
truenas_disk_temperature_celsius{device="sde"} 24
truenas_disk_temperature_celsius{device="sdf"} 23
truenas_disk_temperature_celsius{device="sdg"} 25
truenas_disk_temperature_celsius{device="sdh"} 25
# HELP truenas_disk_write_bytes_per_second Bytes written to disk per second by device
# TYPE truenas_disk_write_bytes_per_second gauge
truenas_disk_write_bytes_per_second{device="nvme0n1"} 0
truenas_disk_write_bytes_per_second{device="sda"} 108782.67392
truenas_disk_write_bytes_per_second{device="sdb"} 94404.59776
truenas_disk_write_bytes_per_second{device="sdc"} 86176.58368
truenas_disk_write_bytes_per_second{device="sdd"} 86131.16928
truenas_disk_write_bytes_per_second{device="sde"} 88192.21504
truenas_disk_write_bytes_per_second{device="sdf"} 80029.9008
truenas_disk_write_bytes_per_second{device="sdg"} 86167.37792
truenas_disk_write_bytes_per_second{device="sdh"} 77948.9792
# HELP truenas_memory_usage_bytes Physical memory usage in bytes
# TYPE truenas_memory_usage_bytes gauge
truenas_memory_usage_bytes{usage="buffers"} 1.4024704e+07
truenas_memory_usage_bytes{usage="cached"} 1.0694528335872e+10
truenas_memory_usage_bytes{usage="free"} 1.7446654181376e+10
truenas_memory_usage_bytes{usage="used"} 5.39221295104e+09
# HELP truenas_network_received_bytes_per_second Bytes received per second by device
# TYPE truenas_network_received_bytes_per_second gauge
truenas_network_received_bytes_per_second{device="eno3"} 73950.45376
truenas_network_received_bytes_per_second{device="eno4"} 25642.52672
truenas_network_received_bytes_per_second{device="vlan10"} 130.6774528
truenas_network_received_bytes_per_second{device="vlan20"} 68536.20736
truenas_network_received_bytes_per_second{device="vlan40"} 0
# HELP truenas_network_sent_bytes_per_second Bytes sent per second by device
# TYPE truenas_network_sent_bytes_per_second gauge
truenas_network_sent_bytes_per_second{device="eno3"} 52076.47232
truenas_network_sent_bytes_per_second{device="eno4"} 20204.48256
truenas_network_sent_bytes_per_second{device="vlan10"} 0
truenas_network_sent_bytes_per_second{device="vlan20"} 52082.688
truenas_network_sent_bytes_per_second{device="vlan40"} 0
# HELP truenas_swap_usage_bytes Swap usage in bytes
# TYPE truenas_swap_usage_bytes gauge
truenas_swap_usage_bytes{usage="free"} 8.577334902784e+09
truenas_swap_usage_bytes{usage="used"} 0
# HELP truenas_system_load System load calculated by processes active at a given time by timeframe
# TYPE truenas_system_load gauge
truenas_system_load{timeframe="longterm"} 0
truenas_system_load{timeframe="midterm"} 0
truenas_system_load{timeframe="shortterm"} 0
# HELP truenas_system_uptime_seconds System uptime in seconds
# TYPE truenas_system_uptime_seconds counter
truenas_system_uptime_seconds 298269
```
