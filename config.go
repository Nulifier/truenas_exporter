package main

import (
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
)

type Config struct {
	TrueNasUrl       string
	TrueNasApiKey    string
	DiskTempInterval int64
}

func NewConfig() *Config {
	return &Config{
		TrueNasUrl:       getEnv("TRUENAS_URL"),
		TrueNasApiKey:    getEnv("TRUENAS_API_KEY"),
		DiskTempInterval: atoi64(getEnvWithDefault("TRUENAS_DISK_TEMP_INTERVAL", "60")),
	}
}

func getEnv(key string) string {
	// Try file first
	if filePath, exists := os.LookupEnv(key + "_FILE"); exists {
		// Check if the file exists
		if fileExists(filePath) {
			value, err := os.ReadFile(filePath)
			if err != nil {
				panic(err)
			}
			return strings.TrimSpace(string(value))
		} else {
			panic(errors.New("File referenced by " + key + "_FILE does not exist"))
		}
	}
	// Try normal variable
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	// variable is required
	panic(errors.New(key + " is a required environmental variable"))
}

func getEnvWithDefault(key string, def string) string {
	// Try file first
	if filePath, exists := os.LookupEnv(key + "_FILE"); exists {
		// Check if the file exists
		if fileExists(filePath) {
			value, err := os.ReadFile(filePath)
			if err != nil {
				panic(err)
			}
			return strings.TrimSpace(string(value))
		} else {
			panic(errors.New("File referenced by " + key + "_FILE does not exist"))
		}
	}
	// Try normal variable
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return def
}

func fileExists(filePath string) bool {
	info, err := os.Stat(filePath)
	if err == nil {
		return !info.IsDir()
	}
	if errors.Is(err, os.ErrNotExist) {
		return false
	}
	panic(err)
}

func atoi64(s string) int64 {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		log.Fatalln("Could not parse integer for configuration")
	}
	return i
}
