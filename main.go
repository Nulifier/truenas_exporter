package main

import (
	"flag"
	"log"
	"net/http"

	_ "github.com/joho/godotenv/autoload"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var addr = flag.String("listen-address", ":9876", "The addres to listen on for HTTP requests")
var verbose = flag.Bool("verbose", false, "Provides extra information while running")

func main() {
	flag.Parse()

	// Load config
	config := NewConfig()

	// Create non-global registry
	reg := prometheus.NewRegistry()
	exporter := NewExporter(config.TrueNasUrl, config.TrueNasApiKey, config.DiskTempInterval, *verbose)
	reg.MustRegister(exporter)

	// Add go runtime metrics and process collectors
	reg.MustRegister(
		collectors.NewGoCollector(),
		collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}),
	)

	// Expose /metrics HTTP endpoint using the regsitry
	http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{Registry: reg}))
	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		http.Redirect(res, req, "/metrics", http.StatusSeeOther)
	})

	log.Printf("Listening on %s", *addr)
	log.Fatal(http.ListenAndServe(*addr, nil))
}
