package main

import (
	"encoding/json"
	"log"
	"slices"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

const namespace = "truenas"

var cpuDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "cpu", "usage_ratio"),
	"Average time the CPU spend in a mode",
	[]string{"mode"},
	nil,
)
var cpuTempDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "cpu", "temperature_celsius"),
	"Temperature of the CPU core",
	[]string{"cpu"},
	nil,
)
var diskReadsDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "disk", "read_bytes_per_second"),
	"Bytes read from disk per second by device",
	[]string{"device"},
	nil,
)
var diskWritesDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "disk", "write_bytes_per_second"),
	"Bytes written to disk per second by device",
	[]string{"device"},
	nil,
)
var networkReceivedDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "network", "received_bytes_per_second"),
	"Bytes received per second by device",
	[]string{"device"},
	nil,
)
var networkSentDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "network", "sent_bytes_per_second"),
	"Bytes sent per second by device",
	[]string{"device"},
	nil,
)
var systemLoadDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "system", "load"),
	"System load calculated by processes active at a given time by timeframe",
	[]string{"timeframe"},
	nil,
)
var memoryUsageDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "memory", "usage_bytes"),
	"Physical memory usage in bytes",
	[]string{"usage"},
	nil,
)
var swapUsageDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "swap", "usage_bytes"),
	"Swap usage in bytes",
	[]string{"usage"},
	nil,
)
var uptimeDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "system", "uptime_seconds"),
	"System uptime in seconds",
	[]string{},
	nil,
)
var arcActualRateDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "arc", "actual_events_per_second"),
	"ZFS actual cache hit/miss rate",
	[]string{"result"},
	nil,
)
var arcRateDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "arc", "events_per_second"),
	"ZFS cache hit/miss rate",
	[]string{"result"},
	nil,
)
var arcSizeDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "arc", "size_bytes"),
	"ZFS ARC size in bytes",
	[]string{},
	nil,
)
var arcResultDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "arc", "result_percentage"),
	"ZFS ARC result percentage by category",
	[]string{"result", "category"},
	nil,
)
var diskTempDesc = prometheus.NewDesc(
	prometheus.BuildFQName(namespace, "disk", "temperature_celsius"),
	"Disk temperature by device in celsius",
	[]string{"device"},
	nil,
)

type Exporter struct {
	truenas          Truenas
	diskTempInterval int64
	verbose          bool
}

func NewExporter(truenasUrl string, truenasApiKey string, diskTempInterval int64, verbose bool) *Exporter {
	return &Exporter{
		truenas:          *NewTruenas(truenasUrl, truenasApiKey),
		diskTempInterval: diskTempInterval,
		verbose:          verbose,
	}
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	if e.verbose {
		log.Println("********************************")
		log.Printf("Collecting metrics from %s\n", e.truenas.url)
	}

	now := time.Now()
	sec := now.Unix()

	// Get the available graphs
	if e.verbose {
		log.Println("Requesting list of graphs")
	}
	data := e.truenas.GetRequest("/reporting/netdata_graphs")
	var graphs []GraphDef
	json.Unmarshal(data, &graphs)

	var graphNames []GraphDataQueryName
	var diskTempGraphNames []GraphDataQueryName

	var graphNameList []string
	for _, graph := range graphs {
		if e.verbose {
			graphNameList = append(graphNameList, graph.Name)
		}
		if graph.Identifiers != nil {
			for _, id := range graph.Identifiers {
				if graph.Name == "disktemp" {
					// Disk temperatures are only checked around every 60s so we
					// need to request a range of times
					diskTempGraphNames = append(diskTempGraphNames, GraphDataQueryName{
						Name:       graph.Name,
						Identifier: id,
					})
				} else {
					graphNames = append(graphNames, GraphDataQueryName{
						Name:       graph.Name,
						Identifier: id,
					})
				}
			}
		} else {
			graphNames = append(graphNames, GraphDataQueryName{
				Name: graph.Name,
			})
		}
	}

	if e.verbose {
		log.Printf("Requesting data for graphs: %s\n", strings.Join(graphNameList, ", "))
	}

	// Issue normal queries
	graphData := e.queryGraphData(GraphDataQuery{
		Graphs: graphNames,
		Query: GraphDataQueryParams{
			Start:     &sec,
			Aggregate: false,
		},
	})

	if e.verbose {
		log.Println("Processing normal graph data into metrics")
	}

	for _, graph := range graphData {
		e.outputMetric(&graph, ch)
	}

	// Issue disktemp queries
	// We use double the interval to ensure that we get at least one sample
	diskTempStart := sec - 2*e.diskTempInterval
	graphData = e.queryGraphData(GraphDataQuery{
		Graphs: diskTempGraphNames,
		Query: GraphDataQueryParams{
			Start:     &diskTempStart,
			End:       &sec,
			Aggregate: false,
		},
	})

	if e.verbose {
		log.Println("Processing disktemp graph data into metrics")
	}

	for _, graph := range graphData {
		e.outputMetric(&graph, ch)
	}

	if e.verbose {
		log.Println("Finished collecting metrics")
	}
}

func (e *Exporter) queryGraphData(query GraphDataQuery) []GraphData {
	queryData, err := json.Marshal(query)
	if err != nil {
		log.Fatalln("Error marshalling query")
	}

	data := e.truenas.PostRequest("/reporting/netdata_get_data", &queryData)
	var graphData []GraphData
	err = json.Unmarshal(data, &graphData)
	if err != nil {
		log.Fatalln("Error unmarshalling graph data")
	}

	return graphData
}

func (e *Exporter) outputMetric(data *GraphData, ch chan<- prometheus.Metric) {
	//log.Printf("Graph data: %+v\n", *data)
	if e.verbose {
		log.Printf("Outputting metric: %s{%s}\n", data.Name, data.Identifier)
	}

	// If there is no data, there is no metric to output
	if len(data.Data) == 0 {
		return
	}

	latest := data.Data[len(data.Data)-1]

	if data.Name == "cpu" {
		// CPU usage by mode
		for idx, mode := range data.Legend[1:] {
			ch <- prometheus.MustNewConstMetric(cpuDesc, prometheus.GaugeValue, latest[idx+1]/100.0, mode)
		}
	} else if data.Name == "cputemp" {
		// CPU temperature by core
		for idx, core := range data.Legend[1:] {
			ch <- prometheus.MustNewConstMetric(cpuTempDesc, prometheus.GaugeValue, latest[idx+1], core)
		}
	} else if data.Name == "disk" {
		readsIndex := slices.Index(data.Legend, "reads")
		writesIndex := slices.Index(data.Legend, "writes")
		if readsIndex == -1 || writesIndex == -1 {
			log.Println("Could not determine disk legend indices")
			return
		}

		// Disk I/O by device in KiB/s
		ch <- prometheus.MustNewConstMetric(diskReadsDesc, prometheus.GaugeValue, latest[readsIndex]*1024.0, data.Identifier)
		ch <- prometheus.MustNewConstMetric(diskWritesDesc, prometheus.GaugeValue, latest[writesIndex]*1024.0, data.Identifier)
	} else if data.Name == "interface" {
		recvIndex := slices.Index(data.Legend, "received")
		sentIndex := slices.Index(data.Legend, "sent")
		if recvIndex == -1 || sentIndex == -1 {
			log.Println("Could not determine interface legend indices")
			return
		}

		// Network I/O by device in KiB/s
		ch <- prometheus.MustNewConstMetric(networkReceivedDesc, prometheus.GaugeValue, latest[recvIndex]*1024.0, data.Identifier)
		ch <- prometheus.MustNewConstMetric(networkSentDesc, prometheus.GaugeValue, latest[sentIndex]*1024.0, data.Identifier)
	} else if data.Name == "load" {
		// System load by timeframe
		for idx, timeframe := range data.Legend[1:] {
			ch <- prometheus.MustNewConstMetric(systemLoadDesc, prometheus.GaugeValue, latest[idx+1], timeframe)
		}
	} else if data.Name == "memory" {
		// Physical memory usage in MiB by usage
		for idx, usage := range data.Legend[1:] {
			ch <- prometheus.MustNewConstMetric(memoryUsageDesc, prometheus.GaugeValue, latest[idx+1]*1024.0*1024.0, usage)
		}
	} else if data.Name == "swap" {
		// Swap usage in MiB by usage
		for idx, usage := range data.Legend[1:] {
			ch <- prometheus.MustNewConstMetric(swapUsageDesc, prometheus.GaugeValue, latest[idx+1]*1024.0*1024.0, usage)
		}
	} else if data.Name == "uptime" {
		// System uptime in seconds
		ch <- prometheus.MustNewConstMetric(uptimeDesc, prometheus.CounterValue, latest[1])
	} else if data.Name == "arcactualrate" {
		// ARC actual hits per second by result
		for idx, result := range data.Legend[1:] {
			ch <- prometheus.MustNewConstMetric(arcActualRateDesc, prometheus.GaugeValue, latest[idx+1], result)
		}
	} else if data.Name == "arcrate" {
		// ARC hits per second by result
		for idx, result := range data.Legend[1:] {
			ch <- prometheus.MustNewConstMetric(arcRateDesc, prometheus.GaugeValue, latest[idx+1], result)
		}
	} else if data.Name == "arcsize" {
		// ARC size in bytes
		ch <- prometheus.MustNewConstMetric(arcSizeDesc, prometheus.CounterValue, latest[1])
	} else if data.Name == "arcresult" {
		// ARC hit percentage by result and category
		for idx, result := range data.Legend[1:] {
			ch <- prometheus.MustNewConstMetric(arcResultDesc, prometheus.GaugeValue, latest[idx+1]/100.0, result, data.Identifier)
		}
	} else if data.Name == "disktemp" {
		// Disk temperature by device
		ch <- prometheus.MustNewConstMetric(diskTempDesc, prometheus.GaugeValue, latest[1], data.Identifier)
	} else {
		log.Printf("Unknown graph type: %s\n", data.Name)
		return
	}
}
