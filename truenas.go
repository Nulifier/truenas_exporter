package main

import (
	"bytes"
	"io"
	"log"
	"net/http"
)

type GraphDef struct {
	Name          string
	Title         string
	VerticalLabel string `json:"vertical_label"`
	Identifiers   []string
}

type GraphDataQueryName struct {
	Name       string `json:"name"`
	Identifier string `json:"identifier,omitempty"`
}

type GraphDataQueryParams struct {
	Unit      *string `json:"unit,omitempty"`
	Page      *int64  `json:"page,omitempty"`
	Start     *int64  `json:"start,omitempty"`
	End       *int64  `json:"end,omitempty"`
	Aggregate bool    `json:"aggregate"`
}

type GraphDataQuery struct {
	Graphs []GraphDataQueryName `json:"graphs"`
	Query  GraphDataQueryParams `json:"reporting_query_netdata"`
}

type GraphData struct {
	Name         string
	Identifier   string
	Data         [][]float64
	Legend       []string
	Start        int64
	End          int64
	Aggregations struct {
		Min  *map[string]float64 `json:"min,omitempty"`
		Mean *map[string]float64 `json:"mean,omitempty"`
		Max  *map[string]float64 `json:"max,omitempty"`
	}
}

type Truenas struct {
	url    string
	apiKey string
}

func NewTruenas(url string, apiKey string) *Truenas {
	return &Truenas{
		url:    url,
		apiKey: apiKey,
	}
}

func (t *Truenas) GetRequest(endpoint string) []byte {
	client := &http.Client{}

	req, err := http.NewRequest("GET", t.getUrl(endpoint), nil)
	if err != nil {
		log.Printf("Error getting metric: %v\n", err)
	}

	req.Header.Add("Authorization", "Bearer "+t.apiKey)

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error getting metric: %v\n", err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading request body: %v\n", err)
	}

	return body
}

func (t *Truenas) PostRequest(endpoint string, body *[]byte) []byte {
	client := &http.Client{}

	var bodyReader *bytes.Reader = nil
	if body != nil {
		bodyReader = bytes.NewReader(*body)
	}

	req, err := http.NewRequest("POST", t.getUrl(endpoint), bodyReader)
	if err != nil {
		log.Println("Error getting metric")
	}

	req.Header.Add("Authorization", "Bearer "+t.apiKey)

	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error getting metric")
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error reading request body")
	}

	return respBody
}

func (t *Truenas) getUrl(endpoint string) string {
	return t.url + "/api/v2.0" + endpoint
}
